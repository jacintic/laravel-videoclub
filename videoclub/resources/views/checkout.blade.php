@extends('layouts.app')
@section('content')
    @php

    $user = session()->get('user');
    $end = session()->get('end');
    $myCart = session()->get('cart');
    if (!isset($end)) {
        $totalMovies = count($myCart);
        $totalMovies = count($myCart);
        $totalPrice = 0;
        foreach ($myCart as $item) {
            $totalPrice += $item->price;
        }
    }
    @endphp
    <section class="container d-flex flex-wrap flex-row checkout">
        <aside>
            <h3>Videoclub</h3>
            @if (!isset($end))
                <p class="totlalprods">Total items purchased: {{ $totalMovies }}</p>
                <p class="totlalprice">Total price to pay: {{ $totalPrice }}€</p>
            @endif
        </aside>
        @if (!isset($user) && !isset($end))
            <section class="container d-flex flex-wrap flex-row checkout col-7">
                @forelse ($myCart as $movie)
                    <div class="card m-3">
                        <img src="{{ $movie->img }}" alt="">
                        <p class="title">{{ $movie->title }}</p>
                        <p class="pmode">{{ $movie->ptype }}</p>
                        <p class="price">{{ $movie->price }}€</p>
                    </div>
                @empty

                @endforelse
            </section>
        @endif
        @if (isset($user) && !isset($end))
            <section class="container d-flex flex-wrap flex-column checkout col-7">
                <p>
                <h5>Shipment data</h5>
                </p>
                <ul>
                    @forelse ($user as $item)

                        <li>Name: {{ $item->name }}</li>
                        <li>Address: {{ $item->address }}</li>
                        <li>Email: {{ $item->email }}</li>
                    @empty

                    @endforelse
                </ul>
            </section>
        @endif
        @if (isset($end))
            <h5 class="mx-auto">
                <p>Thanks for your purchase!</p>
            </h5>
        @endif
        <nav class="d-flex flex-wrap col8 container">
            @if (!isset($end))
                @if (isset($user))
                    <a href="/register/back" type="button" class="btn btn-primary">Prev</a>
                @else
                    <a href="/f" type="button" class="btn btn-primary">Prev</a>
                @endif
                <a href="/{{ isset($user) ? 'end' : 'register' }}"
                    class="next btn btn-primary back ms-auto">{{ isset($user) ? 'Comprar' : 'Next' }}</a>
            @else
                <a href="/f" type="button" class="btn btn-primary">Home</a>
            @endif
        </nav>
    </section>
@endsection
