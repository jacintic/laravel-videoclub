<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    public function scopeGetGens($query)
    {
        return $query->select('movies.genere');
	}
    public function scopeTitle($query, $title)
    {
        return $query->where('title',$title);
	}
    public function scopeGenereWhere($query, $gen)
    {
        return $query->where('genere',$gen);
	}
    public function scopeWhereId($query, $id)
    {
        return $query->where('id',$id);
	}
}
