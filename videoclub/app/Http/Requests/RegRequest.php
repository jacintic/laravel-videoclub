<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|between:8,12',
            'email' => 'required|between:8,15',
            'address' => 'required|between:10,18',
            'pass' => 'required|between:8,12|confirmed',
            'pass_confirmation' => 'required|between:8,12',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'between' => 'The :attribute value :input is not between :min - :max.',
            'required' => 'The :attribute is required',
            'confirmed' => 'The :attribute mus match its confirmation'
        ];
    }
}

