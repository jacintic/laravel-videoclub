<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ShoppingStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $status = $request->session()->get('status');
        $action = $request->session()->get('action');
        $target = $request->session()->get('target');
        switch ($status) {
            case null:
                $status = $request->session()->put('status', 1);
                $action = $request->session()->put('action', 'view');
                $target = $request->session()->put('target', '/checkout');
                break;
            case 1:
                $action = $request->session()->put('action', 'view');
                $target = $request->session()->put('target', '/checkout');
                break;
            case 2:
                $action = $request->session()->put('action', 'redirect');
                $target = $request->session()->put('target', '/register');
                $request->flash();
                break;
            case 3:
                $action = $request->session()->put('action', 'redirect');
                $target = $request->session()->put('target', '/confirmation');
                break;
            case 4:
                $action = $request->session()->put('action', 'view');
                $target = $request->session()->put('target', '/checkout');
                break;

            default:
                $action = $request->session()->put('action', 'view');
                $target = $request->session()->put('target', '/checkout');
                break;
        }

        return $next($request);
    }
}
