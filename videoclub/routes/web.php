<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//mystuff testing DB

//mystuff testing DB
Route::get('/f', [TestController::class, 'load'])->name('loadmovies');

Route::post('/f', [TestController::class, 'insert'])->name('mynameis');

// route search
Route::post('/search', [TestController::class, 'search'])->name('mysearch');
Route::get('/search', [TestController::class, 'searchBack'])->name('mysearch');

// route genere
Route::get('/f/genere/{mgenere}', [TestController::class, 'genere']);

// route buy/rent
Route::get('/purchase/{mid}/{pmode}', [TestController::class, 'cartHome']);

// route empty cart
Route::get('/emptyCart', [TestController::class, 'emptyCart']);

//checkout route (summary)
Route::get('/checkout', [TestController::class, 'checkout'])->middleware('shopping.satus');

//register route
Route::get('/register', [TestController::class, 'register']);
//register POST route
Route::post('/register', [TestController::class, 'registerPost']);
// back button from register => to checkout
Route::get('/checkout/back', [TestController::class, 'checkoutBack']);


// confirmation
Route::get('/confirmation', [TestController::class, 'confirmation']);
// back button from confirmation => to register
Route::get('/register/back', [TestController::class, 'registerBack']);

// end sale
Route::get('/end', [TestController::class, 'end']);
