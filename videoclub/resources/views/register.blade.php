@extends('layouts.app')
@section('content')
    <section class="container d-flex flex-wrap flex-row checkout">
        <form action="/register" method="POST">
            @csrf

            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="inputPassword6" class="col-form-label">Name</label>
                </div>
                <div class="col-auto">
                    <input value="{{old('name')?? ''}}" name="name" type="text" id="inputPassword6" class="form-control"
                        aria-describedby="passwordHelpInline">
                </div>
            </div>

            @error('name')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @enderror


            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="inputPassword6" class="col-form-label">Email</label>
                </div>
                <div class="col-auto">
                    <input value="{{old('email')?? ''}}" name="email" type="email" id="inputPassword6" class="form-control"
                        aria-describedby="passwordHelpInline">
                </div>
            </div>

            @error('email')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @enderror

            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="inputPassword6" class="col-form-label">Address</label>
                </div>
                <div class="col-auto">
                    <input value="{{old('address')?? ''}}" name="address" type="text" id="inputPassword6" class="form-control"
                        aria-describedby="passwordHelpInline">
                </div>
            </div>

            @error('address')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @enderror


            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="inputPassword6" class="col-form-label">Password</label>
                </div>
                <div class="col-auto">
                    <input value="{{old('pass')?? ''}}" name="pass" type="password" id="inputPassword6" class="form-control"
                        aria-describedby="passwordHelpInline">
                </div>
            </div>

            @error('pass')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @enderror

            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="inputPassword6" class="col-form-label">Confirm password</label>
                </div>
                <div class="col-auto">
                    <input value="{{old('pass_confirmation')?? ''}}" name="pass_confirmation" type="password" id="inputPassword6" class="form-control"
                        aria-describedby="passwordHelpInline">
                </div>
            </div>

            @error('pass_confirmation')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @enderror

            <button type="submit" class="btn btn-primary">Submit</button>


        </form>
    </section>
    <a href="/checkout/back" type="button" class="btn btn-primary col-1 ms-3">Back</a>
@endsection
