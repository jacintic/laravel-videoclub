<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Test;
use App\Models\Movie;
use Illuminate\Http\Request;
use App\Http\Requests\RegRequest;
use Illuminate\Support\Facades\DB;




class TestController extends Controller
{
    public function insert(Request $request)
    {
        /*
        $test = Test::create([
            'a' => $request->a
        ]);
        */
        $test = new Test;

        $test->a = $request->a;

        $test->save();



        $tests = DB::table('tests')->get();

        //return redirect()->back()->withInput();

        return view('a')->with('results', $tests);
        //return redirect('/f')->with('results', $tests);

        //return redirect()->back()->with('results',$tests);
    }

    public function search(Request $request)
    {
        $movie = Movie::query();
        $movies = $movie->title($request->title);
        return view('a')->with('movies', $movies->get())->with('generes', $this->getGeneres());
    }
    public function searchBack(Request $request)
    {
        $cart = $request->session()->get('cart', []);
        $mid = $cart[(count($cart) - 1)]->mid;
        $movie = Movie::query();
        $movies = $movie->whereId($mid);
        return view('a')->with('movies', $movies->get())->with('generes', $this->getGeneres());
    }
    public function genere(Request $request)
    {
        $movies = Movie::query();
        $movie = $movies->genereWhere($request->mgenere);
        return view('a')->with('movies', $movie->get())->with('generes', $this->getGeneres());
    }
    public function load(Request $request)
    {
        $end = $request->session()->forget('end');
        $movies = Movie::query();
        return view('a')->with('movies', $movies->get())->with('generes', $this->getGeneres())->with('home', true);
    }
    public function getGeneres()
    {
        return Movie::query()->getGens()->distinct()->get();
    }
    public function cartHome(Request $request)
    {
        $singleMov = Movie::query()->whereId($request->mid)->get();
        $cart = $request->session()->get('cart', []);
        $flickPrice = $request->pmode == 'buy' ? $singleMov[0]->pbuy : $singleMov[0]->prent;
        array_push($cart, (object)['mid' => $request->mid, 'ptype' => $request->pmode, 'title' => $singleMov[0]->title, 'price' => $flickPrice, 'img' => $singleMov[0]->img]);
        $cart = $request->session()->put('cart', $cart);
        return back();
    }
    public function emptyCart(Request $request)
    {
        $request->session()->forget('cart');
        $request->session()->forget('user');
        $request->session()->forget('end');
        return redirect('/f');
    }
    public function checkout(Request $request)
    {
        $action = $request->session()->get('action');
        $target = $request->session()->get('target');
        if($action == 'view')
            return view($target);
        return redirect($target);
    }
    public function register(Request $request)
    {
        $status = $request->session()->get('status');
        $status = $request->session()->put('status', 2);
        return view('register');
    }
    public function registerPost(RegRequest $request)
    {
        $status = $request->session()->get('status');
        $status = $request->session()->put('status', 3);
        // get user data
        $userData = $request->session()->get('user', []);
        array_push($userData, (object)['name' => $request->name, 'address' => $request->address, 'email' => $request->email]);
        $userData = $request->session()->put('user', $userData);

        return redirect('/confirmation');;
    }
    public function checkoutBack(Request $request)
    {
        $status = $request->session()->get('status');
        $status = $request->session()->put('status', 1);
        return redirect('/checkout');
    }
    public function confirmation(Request $request)
    {
        $status = $request->session()->get('status');
        $status = $request->session()->put('status', 4);
        return view('checkout');
    }
    public function registerBack(Request $request)
    {
        $status = $request->session()->get('status');
        $status = $request->session()->put('status', 3);
        $user = $request->session()->forget('user');
        return redirect('/register');
    }
    public function end(Request $request)
    {
        $status = $request->session()->forget('status');
        $user = $request->session()->forget('user');
        $cart = $request->session()->forget('cart');
        $isEnd = $status = $request->session()->get('end');
        $isEnd = $status = $request->session()->put('end', true);
        return view('checkout');
    }
}
