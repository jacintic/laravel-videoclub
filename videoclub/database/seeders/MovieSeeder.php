<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert(
            [
                [
                    'title' => "Mr. Bean's Holidays",
                    'genere' => "comedy",
                    'prent' => 10.50,
                    'pbuy' => 35.50,
                    'img' => '/img/mrb.jpg',
                ],
                [
                    'title' => "Matrix",
                    'genere' => "action",
                    'prent' => 20.50,
                    'pbuy' => 45.50,
                    'img' => '/img/mt.jpg',
                ],
                [
                    'title' => "Matrix II",
                    'genere' => "action",
                    'prent' => 50.50,
                    'pbuy' => 85.50,
                    'img' => '/img/mt2.jpg',
                ],
                [
                    'title' => "Lord of the Rings",
                    'genere' => "fantasy",
                    'prent' => 70.50,
                    'pbuy' => 99.50,
                    'img' => '/img/lotr.jpg',
                ]
            ]
        );
    }
}
