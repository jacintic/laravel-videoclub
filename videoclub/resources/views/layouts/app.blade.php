<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>Document</title>
</head>

<body>
    <div class="container d-flex flex-column flex-wrap mw-100 p-0 m-0 min-vh-100">

        <nav class="navbar navbar-dark bg-dark text-light d-flex justify-content-center overflow-visible"
            style="z-index:2">
            <div class="col-8 d-flex">
                <div class="logo">
                    <a href="/f">
                        <img src="https://download.blender.org/branding/blender_logo.png" alt="" class="logo">
                    </a>
                </div>
                <div class="cart ms-auto col-4 overflow-visible d-flex" style="height:15px;">
                    <div class="items" style="width: 197px;">
                        <!--Number of products: {{ $cart->numitems ?? '0' }}-->
                        @php
                            $myCart = session()->get('cart');
                        @endphp
                        @if (isset($myCart))
                            <div>
                                <p>Num of purchases: {{ count($myCart) }}
                                    <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample"
                                        role="button" aria-expanded="false" aria-controls="collapseExample">
                                        v
                                    </a>
                                </p>
                                <div class="collapse" id="collapseExample" style="width: 376px !important;">
                                    <div class="card card-body text-dark">
                                        @forelse ($myCart as $item)
                                            <p>{{ $item->title }} :
                                                {{ $item->ptype == 'buy' ? 'Bought' : 'Rented' }}.
                                                Price: {{ $item->price }}€</p>
                                        @empty

                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="delete">
                        @if (isset($myCart))
                            <a href="/emptyCart" type="button" class="btn btn-primary">Empty Cart</a>
                        @endif
                    </div>
                    <div class="skip">
                        @if (isset($myCart))
                            <a href="/checkout" type="button" class="btn btn-primary ms-2">Checkout</a>
                        @endif
                    </div>
                </div>
            </div>
        </nav>
        @yield('content')
        <footer class="mt-auto bg-dark p-5">

        </footer>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
