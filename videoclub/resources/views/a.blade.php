@extends('layouts.app')
@section('content')
    <div class="container d-flex flex-wrap flex-row mt-3">
        <div class="row">
            <aside class="col-2">
                <form action="{{ route('mysearch') }}" method="post" class="d-flex flex-wrap flex-column">
                    @csrf
                    <p>Title:</p>
                    <input type="text" name="title" id="" value="{{ old('title') ?? '' }}">
                    <input type="submit" value="Search">
                </form>
                <div>
                    @if (isset($generes))
                        @forelse ($generes as $genere)
                            <p><a
                                    href="/f/genere/{{ $genere->genere ?? '-' }}">{{ ucfirst($genere->genere) ?? '-' }}</a>
                            </p>
                        @empty
                            <p>No generes in the DB!</p>
                        @endforelse
                    @else
                        <p>Oops problems with the DB!</p>
                    @endif
                </div>
            </aside>
            <section class="movies d-flex flex-wrap flex-column justify-content-center col-10">
                @if (isset($home))
                    @php
                        $active = true;
                    @endphp
                    <div class="container">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                @forelse ($movies as $movie)
                                    <div class="carousel-item {{ $active ? 'active' : '' }}">
                                        {{ $active = false }}
                                        <img src="{{ $movie->img }}" class="d-block w-100 mcar"
                                            alt="{{ $movie->title }}">
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                                data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                                data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                @endif
                @if (isset($movies))
                    <div class="container flix mt-3">
                        <div class="row">
                            @forelse ($movies as $movie)
                                <article class="col m-2 shadow p-3 d-flex flex-column">
                                    <img src="{{ $movie->img }}" alt="" class="align-self-center">
                                    <p>{{ $movie->title }}</p>
                                    <p>{{ ucfirst($movie->genere) }}</p>
                                    <div class="d-flex flex-wrap flex-row">
                                        <div class="d-flex">
                                            <p>Price: {{ $movie->pbuy }}</p>
                                            <a href="/purchase/{{ $movie->id }}/buy" type="button"
                                                class="btn btn-primary">Buy</a>
                                        </div>
                                        <div class="d-flex">
                                            <p>Price: {{ $movie->prent }}</p>
                                            <a href="/purchase/{{ $movie->id }}/rent" type="button"
                                                class="btn btn-primary">Rent</a>
                                        </div>
                                    </div>
                                </article>
                            @empty
                                <p>No movies with that title in the DB!</p>
                            @endforelse
                        </div>
                    </div>
                @else
                    <p>Oops problems with the DB!</p>
                @endif
            </section>

        </div>
    </div>

@endsection
